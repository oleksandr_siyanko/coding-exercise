package ua.coding.exercise.exception;


public class RegistrationException extends Exception {

    public RegistrationException() {
    }

    public RegistrationException(String message) {
        super(message);
    }
}
