package ua.coding.exercise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.coding.exercise.exception.RegistrationException;
import ua.coding.exercise.service.RegistrationService;

import java.rmi.RemoteException;

@RestController
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    @RequestMapping("/register")
    public void register(@RequestParam(value = "username") String userName,
                         @RequestParam(value = "password") String password,
                         @RequestParam(value = "dob") String dob,
                         @RequestParam(value = "ssn") String ssn) throws RemoteException {

        try {
            registrationService.register(userName, password, dob, ssn);
        } catch (RegistrationException e) {
            throw new RemoteException(e.getMessage());
        }
    }
}
