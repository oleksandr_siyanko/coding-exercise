package ua.coding.exercise.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.coding.exercise.exception.RegistrationException;
import ua.coding.exercise.model.User;
import ua.coding.exercise.service.ExclusionService;
import ua.coding.exercise.service.RegistrationService;

import java.util.HashSet;
import java.util.Set;

@Service
public class PlatformRegistrationService implements RegistrationService {

    private Set<User> users = new HashSet<User>();

    private final String USER_NAME_PATTERN = "^[a-zA-Z0-9]+$";
    private final String PASS_PATTERN = "^(?=.*[A-Z])(?=.*[0-9]).+${4,}";

    @Autowired
    private ExclusionService exclusionService;

    @Override
    public void register(String username, String pass, String dob, String ssn) throws RegistrationException {

        checkInput(username, pass);
        checkUserInBlacklist(dob, ssn);

        User user = new User(username, pass, dob, ssn);
        checkDuplicatedUser(user);

        users.add(user);
    }

    protected void checkInput(String userName, String pass) throws RegistrationException {
        if (!isValidUserName(userName) || !isValidPass(pass)) {
            throw new RegistrationException("Input values are not valid.");
        }
    }

    protected void checkDuplicatedUser(User user) throws RegistrationException {
        if (users.contains(user)) {
            throw new RegistrationException("The user has been registered with such data.");
        }
    }

    protected void checkUserInBlacklist(String dob, String ssn) throws RegistrationException {
        if (!exclusionService.validate(dob, ssn)) {
            throw new RegistrationException("The user is in black list.");
        }
    }

    protected boolean isValidUserName(String userName) {
        if (userName == null) return false;

        return userName.matches(USER_NAME_PATTERN);
    }

    protected boolean isValidPass(String pass) {
        if (pass == null) return false;

        return pass.matches(PASS_PATTERN);
    }

    public void setExclusionService(ExclusionService exclusionService) {
        this.exclusionService = exclusionService;
    }

    public Set<User> getUsers() {
        return users;
    }
}
