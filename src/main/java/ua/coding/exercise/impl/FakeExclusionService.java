package ua.coding.exercise.impl;

import org.springframework.stereotype.Service;
import ua.coding.exercise.service.ExclusionService;

@Service
public class FakeExclusionService implements ExclusionService {
    @Override
    public boolean validate(String dob, String ssn) {
        return false;
    }
}
