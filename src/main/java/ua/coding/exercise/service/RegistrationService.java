package ua.coding.exercise.service;

import ua.coding.exercise.exception.RegistrationException;

/**
 * Service to register a new user.
 */
public interface RegistrationService {

    /**
     * Conducts validation of a user and registers in case successful validation.
     *
     * @param username is alphanumerical, no spaces value
     * @param pass is at least four characters, at least one upper case character, at least one number
     * @param dob the user's date of birth in ISO 8601 format
     * @param ssn the user's social security number (United States)
     *
     * @throws RegistrationException if one of the cases has been true:
     * - invalid input values
     * - user with the same data has been already registered
     * - user is in blacklist
     */
    void register(String username, String pass, String dob, String ssn) throws RegistrationException;
}
