package ua.coding.exercise.impl;

import org.junit.Test;
import ua.coding.exercise.exception.RegistrationException;
import ua.coding.exercise.model.User;
import ua.coding.exercise.service.ExclusionService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PlatformRegistrationServiceTest {

    @Test
    public void testIsValidUserName() {
        PlatformRegistrationService service = new PlatformRegistrationService();

        assertTrue(service.isValidUserName("a"));
        assertTrue(service.isValidUserName("a1"));
        assertTrue(service.isValidUserName("1"));
        assertTrue(service.isValidUserName("1a"));

        assertFalse(service.isValidUserName(null));
        assertFalse(service.isValidUserName("a 1"));
        assertFalse(service.isValidUserName("a $1"));
    }

    @Test
    public void testIsValidPass() {
        PlatformRegistrationService service = new PlatformRegistrationService();

        assertTrue(service.isValidPass("aA1a"));

        assertFalse(service.isValidPass(null));
        assertFalse(service.isValidPass("a"));
        assertFalse(service.isValidPass("aa"));
        assertFalse(service.isValidPass("aaa"));
        assertFalse(service.isValidPass("aaaa"));
        assertFalse(service.isValidPass("AAAA"));
        assertFalse(service.isValidPass("1111"));
    }

    @Test
    public void testCheckInput() {
        Exception ex = null;
        PlatformRegistrationService service = new PlatformRegistrationService();

        // correct input
        try {
            service.checkInput("aa11", "aA1a");
        } catch (RegistrationException e) {
            ex = e;
        }

        assertNull(ex);


        //incorrect input
        ex = null;
        try {
            service.checkInput(null, "aA1a");
        } catch (RegistrationException e) {
            ex = e;
        }

        assertNotNull(ex);

        //incorrect input
        ex = null;
        try {
            service.checkInput("aa11", null);
        } catch (RegistrationException e) {
            ex = e;
        }

        assertNotNull(ex);
    }

    @Test
    public void testCheckBlacklist() {
        Exception ex = null;
        PlatformRegistrationService service = new PlatformRegistrationService();
        ExclusionService exclusionService = mock(ExclusionService.class);
        service.setExclusionService(exclusionService);
        when(exclusionService.validate(anyString(), anyString()))
                .thenReturn(true)
                .thenReturn(false);
        try {
            service.checkUserInBlacklist("", "");
        } catch (RegistrationException e) {
            ex = e;
        }

        assertNull(ex);

        ex = null;
        try {
            service.checkUserInBlacklist("", "");
        } catch (RegistrationException e) {
            ex = e;
        }
        assertNotNull(ex);
    }

    @Test
    public void testCheckDuplicatedUser() {
        Exception ex = null;
        PlatformRegistrationService service = new PlatformRegistrationService();
        User user = new User("name", "pass", "dob", "ssn");

        //1st add user
        try {
            service.checkDuplicatedUser(user);
        } catch (RegistrationException e) {
            ex = e;
        }
        assertNull(ex);
        service.getUsers().add(user);

        //2nd add user
        try {
            service.checkDuplicatedUser(user);
        } catch (RegistrationException e) {
            ex = e;
        }
        assertNotNull(ex);

    }


}