package ua.coding.exercise.impl;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ua.coding.exercise.exception.RegistrationException;
import ua.coding.exercise.service.ExclusionService;
import ua.coding.exercise.service.RegistrationService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class PlatformRegistrationServiceIntegrationTest extends TestCase {

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private ExclusionService exclusionService;

    @Test
    public void testRegistrationService(){
        assertEquals("class ua.coding.exercise.impl.PlatformRegistrationService", this.registrationService.getClass().toString());
    }

    @Test
    public void testExclusionServiceUsage(){
        Exception ex = null;
        Mockito.when(exclusionService.validate("dob", "ssn")).thenReturn(true);

        try {
            this.registrationService.register("user1", "123456Aa", "dob", "ssn");
        }catch (RegistrationException e){
            ex = e;
        }

        assertNull(ex);
        Mockito.verify(this.exclusionService, VerificationModeFactory.times(1)).validate("dob", "ssn");
    }


    @Configuration
    static class PlatformRegistrationServiceIntegrationTestConfig{

        @Bean
        public RegistrationService registrationService(){
            return new PlatformRegistrationService();
        }

        @Bean
        public ExclusionService exclusionService(){
            return Mockito.mock(ExclusionService.class);
        }
    }
}