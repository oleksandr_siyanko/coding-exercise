package ua.coding.exercise.controller;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ua.coding.exercise.service.RegistrationService;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class RegistrationControllerTest extends TestCase {

    @Autowired
    private RegistrationController controller;

    @Autowired
    private RegistrationService registrationService;


    @Configuration
    static class RegistrationControllerTestConf {

        @Bean
        public RegistrationController registrationController() {
            return new RegistrationController();
        }

        @Bean
        public RegistrationService registrationService() {
            return mock(RegistrationService.class);
        }
    }

    @Test
    public void testHandleRegister() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(this.controller).build();
        mockMvc.perform(post("/register")
                        .param("username", "user1")
                        .param("password", "123456Aa")
                        .param("dob", "12.03.19999")
                        .param("ssn", "123456789")
        ).andExpect(status().isOk());

        Mockito.verify(this.registrationService, VerificationModeFactory.times(1))
                .register("user1", "123456Aa", "12.03.19999", "123456789");
    }

}